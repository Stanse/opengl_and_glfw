#include <iostream>
#include <vector>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

const GLint width = 800;
const GLint height = 600;

const GLchar* vertex_shader_src = "#version 330 core \n"
                                  "layout (location = 0) in vec3 position;\n"
                                  "void main()\n"
                                  "{\n"
                                  "gl_Position = vec4(position.x, position.y, position.z, 1.0);\n"
                                  "}\n\0";

const GLchar* fragment_shader_src = "#version 330 core\n"
                                    "out vec4 color;\n"
                                    "void main()\n"
                                    "{\n"
                                    "color = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
                                    "}\n\0";

struct Vertex
{
  glm::vec3 position;
};

int main()
{
  glfwInit();

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || 1.4.0 Hello triangle", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  glfwSetKeyCallback(window, key_callback);

  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK)
  {
    std::cout << "Failed to inicialize GLEW" << std::endl;
    return -1;
  }

  // Define the viewport dimensions
  GLint width;
  GLint height;
  glfwGetFramebufferSize(window, &width, &height);
  glViewport(0, 0, width, height);

  // Build and compile our shader program
  // Vertex shader
  GLint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex_shader, 1, &vertex_shader_src, nullptr);
  glCompileShader(vertex_shader);
  // Check for compile time errors
  GLint success;
  GLchar infolog[512];
  glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(vertex_shader, 512, nullptr, infolog);
    std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infolog << std::endl;
  }
  // Fragment shader
  GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader, 1, &fragment_shader_src, nullptr);
  glCompileShader(fragment_shader);
  // Check for compile time errors
  glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(fragment_shader, 512, NULL, infolog);
    std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infolog << std::endl;
  }

  // Link shader
  GLint shader_program = glCreateProgram();
  glAttachShader(shader_program, vertex_shader);
  glAttachShader(shader_program, fragment_shader);
  glLinkProgram(shader_program);
  // Check for linking errors
  glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
  if (!success)
  {
    glGetProgramInfoLog(shader_program, 512, NULL, infolog);
    std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infolog << std::endl;
  }
  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);

  // Set up vertex data (and buffer(s)) and attribute pointers
  GLfloat vertices[] = {
    0.5f,  0.5f,  0.0f,  // Верхний правый угол
    0.5f,  -0.5f, 0.0f,  // Нижний правый угол
    -0.5f, -0.5f, 0.0f,  // Нижний левый угол
    -0.5f, 0.5f,  0.0f   // Верхний левый угол
  };
  GLuint indices[] = {
    // Помните, что мы начинаем с 0!
    0, 1, 3,  // Первый треугольник
    1, 2, 3   // Второй треугольник
  };

  std::vector<GLuint> indices1 = { 0, 1, 3, 1, 2, 3 };

  Vertex v1;
  v1.position = glm::vec3(0.5f, 0.5f, 0.0f);
  Vertex v2;
  v2.position = glm::vec3(0.5f, -0.5f, 0.0f);
  Vertex v3;
  v3.position = glm::vec3(-0.5f, -0.5f, 0.0f);
  Vertex v4;
  v4.position = glm::vec3(-0.5f, 0.5f, 0.0f);

  std::vector<Vertex> vertexes{ v1, v2, v3, v4 };

  GLuint VBO, VAO, EBO;
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);
  // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and
  // attribute pointer(s).
  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  //  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glBufferData(GL_ARRAY_BUFFER, vertexes.size() * sizeof(Vertex), &vertexes[0], GL_STATIC_DRAW);

  std::cout << "1. = " << sizeof(vertices) << '\n';
  std::cout << "2. = " << vertexes.size() * sizeof(Vertex) << '\n';
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  //  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices1.size() * sizeof(GLuint), &indices1[0], GL_STATIC_DRAW);

  //  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER,
               0);  // Note that this is allowed, the call to glVertexAttribPointer
                    // registered VBO as the currently bound vertex buffer object so
                    // afterwards we can safely unbind

  glBindVertexArray(0);  // Unbind VAO (it's always a good thing to unbind any
                         // buffer/array to prevent strange bugs), remember: do
                         // NOT unbind the EBO, keep it bound to this VAO

  while (!glfwWindowShouldClose(window))
  {
    // Check if any events have been activiated (key pressed, mouse moved etc.)
    // and call corresponding response functions
    glfwPollEvents();

    // render
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // draw
    //    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glUseProgram(shader_program);
    glBindVertexArray(VAO);
    // glDrawArrays(GL_TRIANGLES, 0, 6);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    glfwSwapBuffers(window);
  }

  glDeleteVertexArrays(1, &VAO);
  glDeleteBuffers(1, &VBO);
  // Terminate GLFW, clearing any resources allocated by GLFW.
  glfwTerminate();
  return 0;
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
}
