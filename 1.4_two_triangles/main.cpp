#include <iostream>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

const GLint width = 800;
const GLint height = 600;

const GLchar* vertex_shader_src = "#version 330 core \n"
                                  "layout (location = 0) in vec3 position;\n"
                                  "void main()\n"
                                  "{\n"
                                  "gl_Position = vec4(position.x, position.y, position.z, 1.0);\n"
                                  "}\n\0";

const GLchar* fragment_shader_src = "#version 330 core\n"
                                    "out vec4 color;\n"
                                    "void main()\n"
                                    "{\n"
                                    "color = vec4(1.0f, 0.0f, 0.0f, 1.0f);\n"
                                    "}\n\0";

const GLchar* fragment_shader_src2 = "#version 330 core\n"
                                     "out vec4 color;\n"
                                     "void main()\n"
                                     "{\n"
                                     "color = vec4(1.0f, 1.0f, 0.0f, 1.0f);\n"
                                     "}\n\0";

int main()
{
  glfwInit();

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || 1.4.1 Two triangles", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  glfwSetKeyCallback(window, key_callback);

  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK)
  {
    std::cout << "Failed to inicialize GLEW" << std::endl;
    return -1;
  }

  // Define the viewport dimensions
  GLint width;
  GLint height;
  glfwGetFramebufferSize(window, &width, &height);
  glViewport(0, 0, width, height);

  // Build and compile our shader program
  // Vertex shader
  GLint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex_shader, 1, &vertex_shader_src, nullptr);
  glCompileShader(vertex_shader);
  // Check for compile time errors
  GLint success;
  GLchar infolog[512];
  glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(vertex_shader, 512, nullptr, infolog);
    std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infolog << std::endl;
  }
  // Fragment shader
  GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader, 1, &fragment_shader_src, nullptr);
  glCompileShader(fragment_shader);
  // Check for compile time errors
  glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(fragment_shader, 512, NULL, infolog);
    std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infolog << std::endl;
  }

  GLuint fragment_shader2 = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader2, 1, &fragment_shader_src2, nullptr);
  glCompileShader(fragment_shader2);
  // Check for compile time errors
  glGetShaderiv(fragment_shader2, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    glGetShaderInfoLog(fragment_shader2, 512, NULL, infolog);
    std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infolog << std::endl;
  }

  GLint shader_program2 = glCreateProgram();
  glAttachShader(shader_program2, vertex_shader);
  glAttachShader(shader_program2, fragment_shader2);
  glLinkProgram(shader_program2);
  // Check for linking errors
  glGetProgramiv(shader_program2, GL_LINK_STATUS, &success);
  if (!success)
  {
    glGetProgramInfoLog(shader_program2, 512, NULL, infolog);
    std::cout << "ERROR::SHADER::PROGRAM2::LINKING_FAILED\n" << infolog << std::endl;
  }

  // Link shader
  GLint shader_program = glCreateProgram();
  glAttachShader(shader_program, vertex_shader);
  glAttachShader(shader_program, fragment_shader);
  glLinkProgram(shader_program);
  // Check for linking errors
  glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
  if (!success)
  {
    glGetProgramInfoLog(shader_program, 512, NULL, infolog);
    std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infolog << std::endl;
  }
  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);
  glDeleteShader(fragment_shader2);

  // Set up vertex data (and buffer(s)) and attribute pointers
  GLfloat vertices1[] = {
    // First triangle
    -0.9f,  -0.5f, 0.0f,  // Left
    -0.0f,  -0.5f, 0.0f,  // Right
    -0.45f, 0.5f,  0.0f,  // Top
  };

  GLfloat vertices2[] = {
    // Second triangle
    0.0f,  -0.5f, 0.0f,  // Left
    0.9f,  -0.5f, 0.0f,  // Right
    0.45f, 0.5f,  0.0f   // Top
  };

  GLuint VAO1, VAO2;
  GLuint VBO1, VBO2;
  glGenVertexArrays(1, &VAO1);
  glGenBuffers(1, &VBO1);

  glGenVertexArrays(1, &VAO2);
  glGenBuffers(1, &VBO2);

  // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and
  // attribute pointer(s).
  glBindVertexArray(VAO1);

  glBindBuffer(GL_ARRAY_BUFFER, VBO1);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), (GLvoid*)0);

  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER,
               0);  // Note that this is allowed, the call to glVertexAttribPointer
  // registered VBO as the currently bound vertex buffer object so
  // afterwards we can safely unbind
  glBindVertexArray(0);  // Unbind VAO (it's always a good thing to unbind any
                         // buffer/array to prevent strange bugs), remember: do
                         // NOT unbind the EBO, keep it bound to this VAO

  // SECOND TRIANGLE

  glBindVertexArray(VAO2);

  glBindBuffer(GL_ARRAY_BUFFER, VBO2);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GL_FLOAT), (GLvoid*)0);

  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  while (!glfwWindowShouldClose(window))
  {
    // Check if any events have been activiated (key pressed, mouse moved etc.)
    // and call corresponding response functions
    glfwPollEvents();

    // render
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // draw
    glUseProgram(shader_program);

    glBindVertexArray(VAO1);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    glUseProgram(shader_program2);
    glBindVertexArray(VAO2);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    glfwSwapBuffers(window);
  }

  glDeleteVertexArrays(1, &VAO1);
  glDeleteBuffers(1, &VBO1);

  glDeleteVertexArrays(1, &VAO2);
  glDeleteBuffers(1, &VBO2);
  // Terminate GLFW, clearing any resources allocated by GLFW.
  glfwTerminate();
  return 0;
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
}
