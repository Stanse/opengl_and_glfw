#version 330 core
precision mediump float;
in vec3 our_color;
in vec4 color_from_pos;
out vec4 pixel_color;

void main()
{
    //pixel_color = vec4(our_color, 1.0f);
    pixel_color = color_from_pos;
}
