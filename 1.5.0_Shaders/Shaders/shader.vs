#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 color;

out vec3 our_color;
out vec4 color_from_pos;

uniform float x_offset;

void main()
{
    gl_Position = vec4(pos.x + x_offset, pos.y, pos.z, 1.0f);
    our_color = color;
    color_from_pos = vec4(pos, 1.0f);
}