#version 330 core
in vec2 TexCoord;

out vec4 color;

uniform vec3 object_color;
uniform vec3 light_color;

void main()
{
    color = vec4(object_color * light_color, 1.0f);
}