// gouraud shading
#version 330 core
out vec4 frag_color;

in vec3 result_color;

uniform vec3 object_color;

void main()
{
    frag_color = vec4(result_color * object_color, 1.0f);
}

//#version 330 core
//in vec3 object_normal;
//in vec3 fragment_position;

//out vec4 color;

//uniform vec3 object_color;
//uniform vec3 light_color;
//uniform vec3 light_position;
//uniform vec3 view_pos;

//void main()
//{
//    // Ambient
//    float ambient_strange = 0.2f;
//    vec3 ambient = ambient_strange * light_color;

//    // Diffuse
//    vec3 norm = normalize(object_normal);
//    vec3 light_direction = normalize(light_position - fragment_position);

//    float diff = max(dot(norm, light_direction), 0.0f);
//    vec3 diffuse = diff * light_color;

//    // Specular
//    float specular_strength = 0.5f;
//    vec3 view_dir = normalize(view_pos - fragment_position);
//    vec3 reflect_dir = reflect(-light_direction, norm);
//    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), 2);
//    vec3 specular = specular_strength * spec * light_color;

//    vec3 result = (ambient + diffuse + specular) * object_color;

//    color = vec4(result, 1.0f);
//}
