// gouraud shading
#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

out vec3 result_color;

uniform vec3 light_pos;
uniform vec3 view_pos;
uniform vec3 light_color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);

    // gouraud shading
    vec3 fragment_position = vec3(model * vec4(position, 1.0f));
    vec3 object_normal = mat3(transpose(inverse(model))) * normal;

    // ambient
    float ambient_strength = 0.1f;
    vec3 ambient = ambient_strength * light_color;

    // diffuse
    vec3 norm = normalize(object_normal);
    vec3 light_dir = normalize(light_pos - position);
    float diff = max(dot(norm, light_dir), 0.0f);
    vec3 diffuse = diff * light_color;

    //specular
    float specular_strength = 1.0f;
    vec3 view_dir = (view_pos - position);
    vec3 reflect_dir = reflect(-light_dir, norm);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0f), 32);
    vec3 specular = specular_strength * spec * light_color;

    result_color = ambient + diffuse + specular;
}

//#version 330 core
//layout (location = 0) in vec3 position;
//layout (location = 1) in vec3 normal;

//out vec3 object_normal;
//out vec3 fragment_position;

//uniform mat4 model;
//uniform mat4 view;
//uniform mat4 projection;

//void main()
//{
//    gl_Position = projection * view * model * vec4(position, 1.0f);
//    object_normal = mat3(transpose(inverse(model))) * normal;
//    fragment_position = vec3(model * vec4(position, 1.0f));
//}
