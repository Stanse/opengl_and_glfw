#include "shader.h"
#include "Camera.h"
#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();
void make_texture(GLuint& texture, const char* path);

// Window dimensions
const GLuint width = 800, height = 600;
GLfloat blend_texture = 0.1f;

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLdouble lastX = 400, lastY = 300;
bool firstMouse = false;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

glm::vec3 light_color(1.0f, 1.0f, 1.0f);
glm::vec3 global_light_pos(1.2f, 1.0f, -1.0f);

// The MAIN function, from here we start the application and run the game loop
int main()
{
  // Init GLFW
  glfwInit();
  // Set all the required options for GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  // Create a GLFWwindow object that we can use for GLFW's functions
  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || 2.2.0 Phong Model", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  // Options
  //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
  glewExperimental = GL_TRUE;
  // Initialize GLEW to setup the OpenGL Function pointers
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, width, height);

  // Build and compile our shader program
  Shader shader("Shaders/shader.vs", "Shaders/shader.frag");

  Shader light_shader("Shaders/light_shader.vs", "Shaders/light_shader.frag");

  float vertices[] = { -0.5f, -0.5f, -0.5f, 0.0f,  0.0f,  -1.0f, 0.5f,  -0.5f, -0.5f, 0.0f,  0.0f,  -1.0f,
                       0.5f,  0.5f,  -0.5f, 0.0f,  0.0f,  -1.0f, 0.5f,  0.5f,  -0.5f, 0.0f,  0.0f,  -1.0f,
                       -0.5f, 0.5f,  -0.5f, 0.0f,  0.0f,  -1.0f, -0.5f, -0.5f, -0.5f, 0.0f,  0.0f,  -1.0f,

                       -0.5f, -0.5f, 0.5f,  0.0f,  0.0f,  1.0f,  0.5f,  -0.5f, 0.5f,  0.0f,  0.0f,  1.0f,
                       0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
                       -0.5f, 0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  -0.5f, -0.5f, 0.5f,  0.0f,  0.0f,  1.0f,

                       -0.5f, 0.5f,  0.5f,  -1.0f, 0.0f,  0.0f,  -0.5f, 0.5f,  -0.5f, -1.0f, 0.0f,  0.0f,
                       -0.5f, -0.5f, -0.5f, -1.0f, 0.0f,  0.0f,  -0.5f, -0.5f, -0.5f, -1.0f, 0.0f,  0.0f,
                       -0.5f, -0.5f, 0.5f,  -1.0f, 0.0f,  0.0f,  -0.5f, 0.5f,  0.5f,  -1.0f, 0.0f,  0.0f,

                       0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.5f,  0.5f,  -0.5f, 1.0f,  0.0f,  0.0f,
                       0.5f,  -0.5f, -0.5f, 1.0f,  0.0f,  0.0f,  0.5f,  -0.5f, -0.5f, 1.0f,  0.0f,  0.0f,
                       0.5f,  -0.5f, 0.5f,  1.0f,  0.0f,  0.0f,  0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

                       -0.5f, -0.5f, -0.5f, 0.0f,  -1.0f, 0.0f,  0.5f,  -0.5f, -0.5f, 0.0f,  -1.0f, 0.0f,
                       0.5f,  -0.5f, 0.5f,  0.0f,  -1.0f, 0.0f,  0.5f,  -0.5f, 0.5f,  0.0f,  -1.0f, 0.0f,
                       -0.5f, -0.5f, 0.5f,  0.0f,  -1.0f, 0.0f,  -0.5f, -0.5f, -0.5f, 0.0f,  -1.0f, 0.0f,

                       -0.5f, 0.5f,  -0.5f, 0.0f,  1.0f,  0.0f,  0.5f,  0.5f,  -0.5f, 0.0f,  1.0f,  0.0f,
                       0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
                       -0.5f, 0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  -0.5f, 0.5f,  -0.5f, 0.0f,  1.0f,  0.0f };

  // World space positions of our cubes
  glm::vec3 cubePositions[] = { glm::vec3(0.0f, 0.0f, 0.0f),    glm::vec3(2.0f, 5.0f, -15.0f),
                                glm::vec3(-1.5f, -2.2f, -2.5f), glm::vec3(-3.8f, -2.0f, -12.3f),
                                glm::vec3(2.4f, -0.4f, -3.5f),  glm::vec3(-1.7f, 3.0f, -7.5f),
                                glm::vec3(1.3f, -2.0f, -2.5f),  glm::vec3(1.5f, 2.0f, -2.5f),
                                glm::vec3(1.5f, 0.2f, -1.5f),   glm::vec3(-1.3f, 1.0f, -1.5f) };

  GLuint VBO, VAO;
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // Position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  // Normal attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(1);

  glBindVertexArray(0);  // Unbind VAO

  GLuint light_VAO;
  glGenVertexArrays(1, &light_VAO);
  glBindVertexArray(light_VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);
  glBindVertexArray(0);  // Unbind light VAO

  glEnable(GL_DEPTH_TEST);

  GLint light_position_loc = glGetUniformLocation(shader.program, "light_position");
  glUniform3f(light_position_loc, global_light_pos.x, global_light_pos.y, global_light_pos.z);
  // Game loop
  while (!glfwWindowShouldClose(window))
  {
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
    glfwPollEvents();
    Do_Movement();

    // Render
    // Clear the color buffer
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Activate shader
    shader.use();
    glBindVertexArray(VAO);
    // Create camera transformation
    glm::mat4 view;
    view = camera.get_look_at_matrix();
    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, (float)width / (float)height, 0.1f, 1000.0f);
    // Get the uniform locations
    GLint modelLoc = glGetUniformLocation(shader.program, "model");
    GLint viewLoc = glGetUniformLocation(shader.program, "view");
    GLint projLoc = glGetUniformLocation(shader.program, "projection");

    GLint objectColorLoc = glGetUniformLocation(shader.program, "object_color");
    GLint lightColorLoc = glGetUniformLocation(shader.program, "light_color");
    GLint light_position_loc = glGetUniformLocation(shader.program, "light_position");
    GLint view_position_loc = glGetUniformLocation(shader.program, "view_pos");

    glUniform3f(objectColorLoc, 1.0f, 0.5f, 0.31f);
    glUniform3f(lightColorLoc, light_color.r, light_color.g, light_color.b);  // зададим цвет источника света (белый)
    glUniform3f(light_position_loc, global_light_pos.x, global_light_pos.y, global_light_pos.z);
    glUniform3f(view_position_loc, camera.position.x, camera.position.y, camera.position.z);

    // Pass the matrices to the shader
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

    // Draw container

    // Calculate the model matrix for each object and pass it to shader before drawing
    glm::mat4 model;
    model = glm::translate(model, cubePositions[0]);
    GLfloat angle = 2.0f * glfwGetTime();
    // model = glm::rotate(model, 0.0f, glm::vec3(1.0f, 0.3f, 0.5f));
    model = glm::scale(model, glm::vec3(5.0f, 0.2f, 5.0f));
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    //    +-------------------------+
    //    |         LIGHT           |
    //    +-------------------------+
    light_shader.use();
    glBindVertexArray(light_VAO);

    glm::mat4 light_view{ camera.get_look_at_matrix() };
    glm::mat4 light_projection{ glm::perspective(camera.zoom, (float)width / (float)height, 0.1f, 1000.0f) };
    glm::mat4 light_model;
    light_model = glm::translate(light_model, global_light_pos);
    light_model = glm::scale(light_model, glm::vec3(0.2f));

    GLint light_model_loc = glGetUniformLocation(light_shader.program, "model");
    GLint light_view_loc = glGetUniformLocation(light_shader.program, "view");
    GLint light_projection_loc = glGetUniformLocation(light_shader.program, "projection");

    glUniformMatrix4fv(light_view_loc, 1, GL_FALSE, glm::value_ptr(light_view));
    glUniformMatrix4fv(light_projection_loc, 1, GL_FALSE, glm::value_ptr(light_projection));

    global_light_pos.x = sin(angle);
    global_light_pos.z = cos(angle);
    light_model = glm::translate(light_model, global_light_pos);
    // light_model = glm::rotate(light_model, 45.0f, glm::vec3(1.0f, 0.3f, 0.5f));
    glUniformMatrix4fv(light_model_loc, 1, GL_FALSE, glm::value_ptr(light_model));
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    // Swap the screen buffers
    glfwSwapBuffers(window);
  }
  // Properly de-allocate all resources once they've outlived their purpose
  glDeleteVertexArrays(1, &VAO);
  glDeleteVertexArrays(1, &light_VAO);
  glDeleteBuffers(1, &VBO);
  // Terminate GLFW, clearing any resources allocated by GLFW.
  glfwTerminate();
  return 0;
}

// Moves/alters the camera positions based on user input
void Do_Movement()
{
  // Camera controls
  if (keys[GLFW_KEY_W])
  {
    camera.check_keyboard(camera_forward, deltaTime);
  }
  if (keys[GLFW_KEY_S])
  {
    camera.check_keyboard(camera_backward, deltaTime);
  }
  if (keys[GLFW_KEY_A])
  {
    camera.check_keyboard(camera_left, deltaTime);
  }
  if (keys[GLFW_KEY_D])
  {
    camera.check_keyboard(camera_right, deltaTime);
  }

  if (keys[GLFW_KEY_UP])
  {
    if (blend_texture < 1.0f)
      blend_texture += 0.01f;
    else
      blend_texture = 1.0f;
  }
  if (keys[GLFW_KEY_DOWN])
  {
    if (blend_texture > 0.0f)
      blend_texture -= 0.01f;
    else
      blend_texture = 0.0f;
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  if (key >= 0 && key < 1024)
  {
    if (action == GLFW_PRESS)
      keys[key] = true;
    else if (action == GLFW_RELEASE)
      keys[key] = false;
  }
}

void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{
  camera.check_mouse_scroll(yoffset);
}

void mouse_callback(GLFWwindow* /*window*/, double xpos, double ypos)
{
  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

  lastX = xpos;
  lastY = ypos;

  camera.check_mouse_movement(xoffset, yoffset);
}

void make_texture(GLuint& texture, const char* path)
{
  glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load at path." << std::endl;
    stbi_image_free(data);
  }
}
