#version 330 core
in vec3 object_normal;
in vec3 fragment_position;
in vec2 texture_coordinates;

out vec4 color;

uniform vec3 view_pos;

struct Material
{
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
};

struct Light
{
    vec3 position;
    vec3  direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;

    float cut_off;
    float outer_cut_off;
};

uniform Material material;
uniform  Light light;

void main()
{
    // Diffuse
    vec3 norm = normalize(object_normal);
    vec3 light_direction = normalize(light.position - fragment_position);
    float diff = max(dot(norm, light_direction), 0.0f);
    vec3 diffuse = light.diffuse * diff * vec3(texture2D(material.diffuse, texture_coordinates));

    // Ambient
    vec3 ambient = light.ambient * diffuse;

    // Specular
    float specular_strength = 0.5f;
    vec3 view_dir = normalize(view_pos - fragment_position);
    vec3 reflect_dir = reflect(-light_direction, norm);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
    vec3 specular = light.specular * spec * vec3(texture2D(material.specular, texture_coordinates));

    // spotlight (soft edges)
    float theta = dot(light_direction, normalize(-light.direction));
    float epsilon = (light.cut_off - light.outer_cut_off);
    float intensity = clamp((theta - light.outer_cut_off) / epsilon, 0.0, 1.0);
    diffuse *= intensity;
    specular *= intensity;

    // attenuation
    float distance = length(light.position - fragment_position);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic* distance*distance);
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    vec3 result = ambient + diffuse + specular;

    color = vec4(result, 1.0f);

}
