#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();
// GLuint make_texture(const char* path);

// Window dimensions
const GLuint width = 1400, height = 800;
GLfloat blend_texture = 0.1f;

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLdouble lastX = 400, lastY = 300;
bool firstMouse = false;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

glm::vec3 light_color(1.0f, 1.0f, 1.0f);
glm::vec3 global_light_pos(1.2f, 1.0f, -1.0f);

// The MAIN function, from here we start the application and run the game loop
int main()
{
  // Init GLFW
  glfwInit();
  // Set all the required options for GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  // Create a GLFWwindow object that we can use for GLFW's functions
  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || 3.1.0 Assimp lib", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  // Options
  //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
  glewExperimental = GL_TRUE;
  // Initialize GLEW to setup the OpenGL Function pointers
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, width, height);

  // Build and compile our shader program
  Shader shader("Shaders/shader.vs", "Shaders/shader.frag");

  //  Shader light_shader("Shaders/light_shader.vs", "Shaders/light_shader.frag");

  std::vector<GLuint> indices = {
    // Note that we start from 0!
    0, 1, 2,  // First Triangle
    0, 2, 3   // Second Triangle
  };

  //===============================================================

  //  GLuint diffuse_map = make_texture("backpack/texture_diffuse1.png");
  //  GLuint specular_map = make_texture("texture_specular1.png");

  //  Texture texture1;
  //  texture1.id = diffuse_map;
  //  texture1.type = "texture_diffuse";
  //  texture1.path = "texture_diffuse1.png";

  //  Texture texture2;
  //  texture2.id = specular_map;
  //  texture2.type = "texture_specular";
  //  texture2.path = "texture_specular1.png";

  //  std::vector<Texture> textures{ texture1 };

  Vertex vertex1;
  vertex1.position = glm::vec3(-0.5f, -0.5f, -0.5f);
  vertex1.normal = glm::vec3(0.0f, 0.0f, -1.0f);
  vertex1.texture_coordinates = glm::vec2(0.0f, 0.0f);

  Vertex vertex2;
  vertex2.position = glm::vec3(0.5f, -0.5f, -0.5f);
  vertex2.normal = glm::vec3(0.0f, 0.0f, -1.0f);
  vertex2.texture_coordinates = glm::vec2(1.0f, 0.0f);

  Vertex vertex3;
  vertex3.position = glm::vec3(0.5f, 0.5f, -0.5f);
  vertex3.normal = glm::vec3(0.0f, 0.0f, -1.0f);
  vertex3.texture_coordinates = glm::vec2(1.0f, 1.0f);

  Vertex vertex4;
  vertex4.position = glm::vec3(-0.5f, 0.5f, -0.5f);
  vertex4.normal = glm::vec3(0.0f, 0.0f, -1.0f);
  vertex4.texture_coordinates = glm::vec2(0.0f, 1.0f);

  std::vector<Vertex> verts = { vertex1, vertex2, vertex3, vertex4 };

  //  Mesh mesh(verts, indices, textures);
  //  Model game_model("cube/cube.obj");
  //  Model game_model("backpack/backpack.obj");
  Model writer("models/writer/1930-type-typewriter.obj");
  Model table("models/table/table.obj");
  //  Model floor("floor/floor.obj");
  Model lamp("models/lamp/lamp.obj");

  camera.position = glm::vec3(2.85f, 6.68f, 4.03f);
  camera.yaw = -134.75;
  camera.pitch = -57.5;
  camera.check_mouse_movement(0, 0);

  glEnable(GL_DEPTH_TEST);

  //      +-------------------------+
  //      |         Game loop       |
  //      +-------------------------+

  while (!glfwWindowShouldClose(window))
  {
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
    glfwPollEvents();
    Do_Movement();

    // Clear the color buffer
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //    +-------------------------+
    //    |   LIGHT SETTINGS        |
    //    +-------------------------+
    shader.set_float("material.shininess", 32.0f);

    // directional light
    shader.set_vec3("direct_light.direction", -0.2f, -1.0f, -0.3f);

    shader.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("direct_light.diffuse", 0.4f, 0.4f, 0.4f);  // darken the light a bit to fit the scene
    shader.set_vec3("direct_light.specular", 0.5f, 0.5f, 0.5f);

    shader.use();

    //    +-------------------------+
    //    |   VIEW                  |
    //    +-------------------------+
    glm::mat4 view;
    view = camera.get_look_at_matrix();

    GLint view_loc = glGetUniformLocation(shader.program, "view");
    glUniformMatrix4fv(view_loc, 1, GL_FALSE, glm::value_ptr(view));

    shader.set_vec3("view_pos", camera.position.x, camera.position.y, camera.position.z);

    //    +-------------------------+
    //    |   PROJECTION            |
    //    +-------------------------+
    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, (float)width / (float)height, 0.1f, 1000.0f);

    GLint projection_loc = glGetUniformLocation(shader.program, "projection");
    glUniformMatrix4fv(projection_loc, 1, GL_FALSE, glm::value_ptr(projection));

    //    +-------------------------+
    //    |   MODELS                |
    //    +-------------------------+
    GLint model_loc = glGetUniformLocation(shader.program, "model");

    //    glm::mat4 floor_model_pos;
    //    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(floor_model_pos));
    //    floor.draw(shader);

    glm::mat4 table_model_pos;
    table_model_pos = glm::translate(table_model_pos, glm::vec3(0.0f, 0.0f, 0.0f));
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(table_model_pos));
    table.draw(shader);

    glm::mat4 writer_model_pos;
    writer_model_pos = glm::scale(writer_model_pos, glm::vec3(0.15f));
    writer_model_pos = glm::translate(writer_model_pos, glm::vec3(-0.5f, 10.0f, 0.0f));
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(writer_model_pos));
    writer.draw(shader);

    // spotLight
    GLfloat angle = 2.0f * glfwGetTime();
    angle = cos(angle);

    shader.set_vec3("spot_light.position", 0.500197f, 3.63321f, -0.0114202f);
    shader.set_vec3("spot_light.direction", angle, -1.0f, -0.03378f);
    shader.set_vec3("spot_light.ambient", 0.0f, 0.0f, 0.0f);
    shader.set_vec3("spot_light.diffuse", 1.0f, 1.0f, 1.0f);
    shader.set_vec3("spot_light.specular", 1.0f, 1.0f, 1.0f);
    shader.set_float("spot_light.constant", 1.0f);
    shader.set_float("spot_light.linear", 0.09);
    shader.set_float("spot_light.quadratic", 0.032);
    shader.set_float("spot_light.cut_off", glm::cos(glm::radians(12.5f)));
    shader.set_float("spot_light.outer_cut_off", glm::cos(glm::radians(15.0f)));

    glm::mat4 lamp_model_pos;
    lamp_model_pos = glm::translate(lamp_model_pos, glm::vec3(.5f, 5.5f, 0.0f));
    lamp_model_pos = glm::scale(lamp_model_pos, glm::vec3(1.0f));
    lamp_model_pos = glm::rotate(lamp_model_pos, angle * 20.0f, glm::vec3(0.0f, 0.0f, 1.0f));
    glUniformMatrix4fv(model_loc, 1, GL_FALSE, glm::value_ptr(lamp_model_pos));
    lamp.draw(shader);

    // Swap the screen buffers
    glfwSwapBuffers(window);

    camera.print_position();
  }

  // Terminate GLFW, clearing any resources allocated by GLFW.
  glfwTerminate();
  return 0;
}

GLuint make_texture(const char* path)
{
  GLuint texture;
  glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load!!!!!!" << std::endl;
    stbi_image_free(data);
  }

  return texture;
}

// Moves/alters the camera positions based on user input
void Do_Movement()
{
  // Camera controls
  if (keys[GLFW_KEY_W])
  {
    camera.check_keyboard(camera_forward, deltaTime);
  }
  if (keys[GLFW_KEY_S])
  {
    camera.check_keyboard(camera_backward, deltaTime);
  }
  if (keys[GLFW_KEY_A])
  {
    camera.check_keyboard(camera_left, deltaTime);
  }
  if (keys[GLFW_KEY_D])
  {
    camera.check_keyboard(camera_right, deltaTime);
  }

  if (keys[GLFW_KEY_UP])
  {
    if (blend_texture < 1.0f)
      blend_texture += 0.01f;
    else
      blend_texture = 1.0f;
  }
  if (keys[GLFW_KEY_DOWN])
  {
    if (blend_texture > 0.0f)
      blend_texture -= 0.01f;
    else
      blend_texture = 0.0f;
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  if (key >= 0 && key < 1024)
  {
    if (action == GLFW_PRESS)
      keys[key] = true;
    else if (action == GLFW_RELEASE)
      keys[key] = false;
  }
}

void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{
  camera.check_mouse_scroll(yoffset);
}

void mouse_callback(GLFWwindow* /*window*/, double xpos, double ypos)
{
  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

  lastX = xpos;
  lastY = ypos;

  camera.check_mouse_movement(xoffset, yoffset);
}
