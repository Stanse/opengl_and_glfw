#version 330 core
layout (location = 0) in vec2 a_position;
layout (location = 1) in vec3 a_color;

out vec3 f_color;

uniform vec2 offsets[100];

void main()
{
    vec2 offset = offsets[gl_InstanceID];
    gl_Position = vec4(a_position + offset, 0.0, 1.0);
    f_color = a_color;
}
