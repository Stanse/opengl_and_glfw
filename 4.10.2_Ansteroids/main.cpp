#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include <GLFW/glfw3.h>
#include <map>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();
GLuint loadCubemap(std::vector<std::string> faces);
// GLuint make_texture(const char* path);

GLboolean debug_mode = false;

// Window dimensions
const GLuint width = 1400, height = 800;
GLfloat blend_texture = 0.1f;

// Camera
Camera camera(glm::vec3(10.0f, 0.0f, 10.0f));
bool keys[1024];
GLdouble lastX = 400, lastY = 300;
bool firstMouse = false;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

glm::vec3 light_color(1.0f, 1.0f, 1.0f);
glm::vec3 global_light_pos(1.2f, 1.0f, -1.0f);

// The MAIN function, from here we start the application and run the game loop
int main()
{
  // Init GLFW
  glfwInit();
  // Set all the required options for GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  glfwWindowHint(GLFW_SAMPLES, 16);  // MSAA Anti aliasing

  // Create a GLFWwindow object that we can use for GLFW's functions
  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || 4.10.2 Asteroids", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  // Options
  //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
  glewExperimental = GL_TRUE;
  // Initialize GLEW to setup the OpenGL Function pointers
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, width, height);

  // Build and compile our shader program
  Shader shader("Shaders/shader.vs", "Shaders/shader.fs");
  Shader shader_skybox("Shaders/shader_skybox.vs", "Shaders/shader_skybox.fs");
  Shader shader_asteroid("Shaders/shader_asteroid.vs", "Shaders/shader_asteroid.fs");
  Shader shader_debug("Shaders/shader_debug.vs", "Shaders/shader_debug.fs", "Shaders/shader_debug.gs");

  float skyboxVertices[] = { // positions
                             -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f,
                             1.0f,  -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f,

                             -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f,
                             -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f, 1.0f,

                             1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  1.0f,  1.0f,
                             1.0f,  1.0f,  1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f, -1.0f,

                             -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  1.0f,  1.0f,  1.0f,
                             1.0f,  1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f, -1.0f, 1.0f,

                             -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f,  1.0f,  1.0f,
                             1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f,

                             -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f,
                             1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f
  };

  GLuint VAOskybox, VBOskybox;
  glGenVertexArrays(1, &VAOskybox);
  glGenBuffers(1, &VBOskybox);

  glBindVertexArray(VAOskybox);
  glBindBuffer(GL_ARRAY_BUFFER, VBOskybox);
  glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);

  //      +-------------------------+
  //      |         Load models     |
  //      +-------------------------+
  Model planet("../models/planet/planet.obj");
  Model asteroid("../models/asteroid/asteroid.obj");

  unsigned int amount = 50000;
  glm::mat4* modelMatrices;
  modelMatrices = new glm::mat4[amount];
  srand(glfwGetTime());  // задаем seed для генератора случ. чисел
  float radius = 60.0;
  float offset = 25.0f;
  for (unsigned int i = 0; i < amount; i++)
  {
    glm::mat4 model(1.0f);
    // 1. перенос: расположить вдоль окружности радиусом 'radius'
    // и добавить смещение в пределах [-offset, offset]
    float angle = (float)i / (float)amount * 360.0f;
    float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
    float x = sin(angle) * radius + displacement;
    displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
    // высоту поля держим заметно меньшей, чем размеры в плоскости XZ
    float y = displacement * 0.4f;
    displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
    float z = cos(angle) * radius + displacement;
    model = glm::translate(model, glm::vec3(x, y, z));

    // 2. масштабирование: случайное масштабирование в пределах (0.05, 0.25f)
    float scale = (rand() % 20) / 100.0f + 0.05;
    model = glm::scale(model, glm::vec3(scale));

    // 3. поворот: поворот на случайный угол вдоль
    float rotAngle = (rand() % 360);
    model = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8f));

    // 4. добавляем в массив матриц
    modelMatrices[i] = model;
  }

  GLuint VBO_asteroids;
  glGenBuffers(1, &VBO_asteroids);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_asteroids);
  glBufferData(GL_ARRAY_BUFFER, amount * sizeof(glm::mat4), &modelMatrices[0], GL_STATIC_DRAW);

  for (GLuint i = 0; i < asteroid.meshes.size(); ++i)
  {
    GLuint VAO = asteroid.meshes[i].VAO;
    glBindVertexArray(VAO);
    GLsizei vec4_size = sizeof(glm::vec4);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4_size, (GLvoid*)0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4_size, (GLvoid*)(vec4_size));
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4_size, (GLvoid*)(2 * vec4_size));
    glEnableVertexAttribArray(5);
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4_size, (GLvoid*)(3 * vec4_size));
    glEnableVertexAttribArray(6);

    glVertexAttribDivisor(3, 1);
    glVertexAttribDivisor(4, 1);
    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);

    glBindVertexArray(0);
  }

  std::vector<std::string> faces{ "../models/skybox_space/right.png", "../models/skybox_space/left.png",
                                  "../models/skybox_space/top.png",   "../models/skybox_space/bottom.png",
                                  "../models/skybox_space/front.png", "../models/skybox_space/back.png" };
  unsigned int cubemap_texture = loadCubemap(faces);

  shader_skybox.use();
  shader_skybox.set_int("skybox", 0);

  camera.position = glm::vec3(36.2f, 4.18f, 16.7f);
  camera.yaw = -138.5;
  camera.pitch = -16;
  camera.movement_speed = 10;
  camera.check_mouse_movement(0, 0);

  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);
  //  glEnable(GL_BLEND);
  //  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glEnable(GL_MULTISAMPLE);  // set MSAA

  //      +-------------------------+
  //      |         Game loop       |
  //      +-------------------------+
  while (!glfwWindowShouldClose(window))
  {
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
    glfwPollEvents();
    Do_Movement();

    // Clear the color buffer
    if (debug_mode)
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    }
    else
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //    +-------------------------+
    //    |   LIGHT SETTINGS        |
    //    +-------------------------+
    shader.use();
    shader.set_float("material.shininess", 32.0f);

    // directional light
    shader.set_vec3("direct_light.direction", -0.2f, -1.0f, -1.3f);

    shader.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("direct_light.diffuse", 0.4, 0.4, 0.4);  // darken the light a bit to fit the scene
    shader.set_vec3("direct_light.specular", 0.5f, 0.5f, 0.5f);

    shader_asteroid.use();
    shader_asteroid.set_float("material.shininess", 32.0f);

    // directional light
    shader_asteroid.set_vec3("direct_light.direction", -0.2f, -1.0f, -1.3f);

    shader_asteroid.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader_asteroid.set_vec3("direct_light.diffuse", 0.4f, 0.4f, 0.4f);  // darken the light a bit to fit the scene
    shader_asteroid.set_vec3("direct_light.specular", 0.5f, 0.5f, 0.5f);

    //    +-------------------------+
    //    |   VIEW                  |
    //    +-------------------------+
    glm::mat4 view;
    view = camera.get_look_at_matrix();

    shader.use();
    shader.set_mat4("view", view);
    shader.set_vec3("view_pos", camera.position.x, camera.position.y, camera.position.z);

    shader_asteroid.use();
    shader_asteroid.set_mat4("view", view);
    shader_asteroid.set_vec3("view_pos", camera.position.x, camera.position.y, camera.position.z);

    //    +-------------------------+
    //    |   PROJECTION            |
    //    +-------------------------+
    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, (float)width / (float)height, 0.1f, 1000.0f);

    shader.use();
    shader.set_mat4("projection", projection);

    shader_asteroid.use();
    shader_asteroid.set_mat4("projection", projection);

    //    +-------------------------+
    //    |   MODELS                |
    //    +-------------------------+
    shader.use();

    GLfloat time = glfwGetTime();
    glm::mat4 planet_model_pos;
    planet_model_pos = glm::translate(planet_model_pos, glm::vec3(0.0f, 0.0f, 0.0f));
    planet_model_pos = glm::scale(planet_model_pos, glm::vec3(5.0f));
    planet_model_pos = glm::rotate(planet_model_pos, time * 20, glm::vec3(0.0f, 0.8f, 0.0f));
    shader.set_mat4("model", planet_model_pos);
    planet.draw(shader);

    if (debug_mode)
    {
      shader_debug.use();
      shader_debug.set_mat4("projection", projection);
      shader_debug.set_mat4("view", view);
      shader_debug.set_mat4("model", planet_model_pos);
      planet.draw(shader_debug);
    }

    //// рендер метеоритов
    for (unsigned int i = 0; i < amount; i++)
    {
      glm::vec3 pos(sin(time), 1.0f, cos(time));
      glm::mat4 model = modelMatrices[i];
      model = glm::translate(model, pos);
      shader.set_mat4("model", model);
      asteroid.draw(shader);

      if (debug_mode)
      {
        shader_debug.use();
        shader_debug.set_mat4("projection", projection);
        shader_debug.set_mat4("view", view);
        shader_debug.set_mat4("model", model);
        asteroid.draw(shader_debug);
        shader.use();
      }
    }

    //    shader_asteroid.use();
    //    shader.set_int("material.texture_diffuse1", 0);
    //    glActiveTexture(GL_TEXTURE0);
    //    glBindTexture(GL_TEXTURE_2D, asteroid.textures_loaded[0].id);  // note: we also made the textures_loaded
    //    for (unsigned int i = 0; i < asteroid.meshes.size(); i++)
    //    {
    //      glBindVertexArray(asteroid.meshes[i].VAO);
    //      glDrawElementsInstanced(GL_TRIANGLES, asteroid.meshes[i].indices.size(), GL_UNSIGNED_INT, 0, amount);
    //      glBindVertexArray(0);
    //    }

    //  | draw skybox
    //  |-------------
    glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's
                             // content
    shader_skybox.use();
    view = glm::mat4(glm::mat3(camera.get_look_at_matrix()));  // remove translation from the view matrix
    shader_skybox.set_mat4("view", view);
    shader_skybox.set_mat4("projection", projection);
    // skybox cube
    glBindVertexArray(VAOskybox);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_texture);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
    glDepthFunc(GL_LESS);  // set depth function back to default

    // Swap the screen buffers
    glEnable(GL_FRAMEBUFFER_SRGB);  // Gamma correction
    glfwSwapBuffers(window);
    glDisable(GL_FRAMEBUFFER_SRGB);

    //    +-------------------------+
    //    |   LOG                   |
    //    +-------------------------+
    if (debug_mode)
      camera.print_position();
  }

  // Terminate GLFW, clearing any resources allocated by GLFW.
  glfwTerminate();
  return 0;
}

GLuint make_texture(const char* path)
{
  GLuint texture;
  glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load!!!!!!" << std::endl;
    stbi_image_free(data);
  }

  return texture;
}

// Moves/alters the camera positions based on user input
void Do_Movement()
{
  // Camera controls
  if (keys[GLFW_KEY_W])
  {
    camera.check_keyboard(camera_forward, deltaTime);
  }
  if (keys[GLFW_KEY_S])
  {
    camera.check_keyboard(camera_backward, deltaTime);
  }
  if (keys[GLFW_KEY_A])
  {
    camera.check_keyboard(camera_left, deltaTime);
  }
  if (keys[GLFW_KEY_D])
  {
    camera.check_keyboard(camera_right, deltaTime);
  }

  if (keys[GLFW_KEY_F1])
  {
    debug_mode = false;
  }
  if (keys[GLFW_KEY_F2])
  {
    debug_mode = true;
  }

  if (keys[GLFW_KEY_UP])
  {
    if (blend_texture < 1.0f)
      blend_texture += 0.01f;
    else
      blend_texture = 1.0f;
  }
  if (keys[GLFW_KEY_DOWN])
  {
    if (blend_texture > 0.0f)
      blend_texture -= 0.01f;
    else
      blend_texture = 0.0f;
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  if (key >= 0 && key < 1024)
  {
    if (action == GLFW_PRESS)
      keys[key] = true;
    else if (action == GLFW_RELEASE)
      keys[key] = false;
  }
}

void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{
  camera.check_mouse_scroll(yoffset);
}

void mouse_callback(GLFWwindow* /*window*/, double xpos, double ypos)
{
  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

  lastX = xpos;
  lastY = ypos;

  camera.check_mouse_movement(xoffset, yoffset);
}

GLuint loadCubemap(std::vector<std::string> faces)
{
  unsigned int textureID;
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

  int width, height, nrChannels;
  for (unsigned int i = 0; i < faces.size(); i++)
  {
    unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
    if (data)
    {
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
      stbi_image_free(data);
    }
    else
    {
      std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
      stbi_image_free(data);
    }
  }
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  return textureID;
}
