#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texture_coords;
layout (location = 3) in mat4 instance_matrix;

out VS_OUT
{
    vec3 object_normal;
    vec3 fragment_position;
    vec2 texture_coordinates;
} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
    vs_out.object_normal = mat3(transpose(inverse(model))) * normal;
//    vs_out.object_normal = normal;
    vs_out.fragment_position = vec3(model * vec4(position, 1.0f));
    vs_out.texture_coordinates = texture_coords;
}
