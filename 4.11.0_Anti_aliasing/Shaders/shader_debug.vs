#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texture_coords;

out VS_OUT {
    vec3 g_normal;
} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
//    vs_out.g_normal = mat3(transpose(inverse(model))) * normal;

    mat3 normalMatrix = mat3(transpose(inverse(view * model)));
    vs_out.g_normal = vec3(projection * vec4(normalMatrix * normal, 0.0));
}
