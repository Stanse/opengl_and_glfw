cmake_minimum_required(VERSION 3.16)

project(4.2.0_Stencil_buffer LANGUAGES CXX C)

add_executable(4.2.0_Stencil_buffer
    main.cpp
    Shader.h
    stb_image.h
    Camera.h
    )

target_compile_features(4.2.0_Stencil_buffer PUBLIC cxx_std_17)

find_package(GLEW REQUIRED)
find_package(glfw3 REQUIRED)
find_package(assimp REQUIRED)

target_include_directories(4.2.0_Stencil_buffer PUBLIC
    ${GLEW_INCLUDE_DIRS}
    ${ASSIMP_INCLUDE_DIRS}
    )

target_link_libraries(4.2.0_Stencil_buffer PRIVATE
    ${GLEW_LIBRARIES}
    -lglfw
    -lGL
    -lassimp
    )
