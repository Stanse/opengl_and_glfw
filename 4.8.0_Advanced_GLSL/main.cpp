#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include <GLFW/glfw3.h>
#include <map>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();
GLuint loadCubemap(std::vector<std::string> faces);
// GLuint make_texture(const char* path);

// Window dimensions
const GLuint width = 1400, height = 800;
GLfloat blend_texture = 0.1f;

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLdouble lastX = 400, lastY = 300;
bool firstMouse = false;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

glm::vec3 light_color(1.0f, 1.0f, 1.0f);
glm::vec3 global_light_pos(1.2f, 1.0f, -1.0f);

// The MAIN function, from here we start the application and run the game loop
int main()
{
  // Init GLFW
  glfwInit();
  // Set all the required options for GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  // Create a GLFWwindow object that we can use for GLFW's functions
  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || 4.8 Advansed GLSL", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  // Options
  //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
  glewExperimental = GL_TRUE;
  // Initialize GLEW to setup the OpenGL Function pointers
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, width, height);

  // Build and compile our shader program
  Shader shader_red("Shaders/common.vs", "Shaders/red.fs");
  Shader shader_green("Shaders/common.vs", "Shaders/green.fs");
  Shader shader_blue("Shaders/common.vs", "Shaders/blue.fs");
  Shader shader_yellow("Shaders/common.vs", "Shaders/yellow.fs");

  //      +-------------------------+
  //      |         Load models     |
  //      +-------------------------+
  float cubeVertices[] = {
    // positions
    -0.5f, -0.5f, -0.5f, 0.5f,  -0.5f, -0.5f, 0.5f,  0.5f,  -0.5f,
    0.5f,  0.5f,  -0.5f, -0.5f, 0.5f,  -0.5f, -0.5f, -0.5f, -0.5f,

    -0.5f, -0.5f, 0.5f,  0.5f,  -0.5f, 0.5f,  0.5f,  0.5f,  0.5f,
    0.5f,  0.5f,  0.5f,  -0.5f, 0.5f,  0.5f,  -0.5f, -0.5f, 0.5f,

    -0.5f, 0.5f,  0.5f,  -0.5f, 0.5f,  -0.5f, -0.5f, -0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f, -0.5f, -0.5f, 0.5f,  -0.5f, 0.5f,  0.5f,

    0.5f,  0.5f,  0.5f,  0.5f,  0.5f,  -0.5f, 0.5f,  -0.5f, -0.5f,
    0.5f,  -0.5f, -0.5f, 0.5f,  -0.5f, 0.5f,  0.5f,  0.5f,  0.5f,

    -0.5f, -0.5f, -0.5f, 0.5f,  -0.5f, -0.5f, 0.5f,  -0.5f, 0.5f,
    0.5f,  -0.5f, 0.5f,  -0.5f, -0.5f, 0.5f,  -0.5f, -0.5f, -0.5f,

    -0.5f, 0.5f,  -0.5f, 0.5f,  0.5f,  -0.5f, 0.5f,  0.5f,  0.5f,
    0.5f,  0.5f,  0.5f,  -0.5f, 0.5f,  0.5f,  -0.5f, 0.5f,  -0.5f,
  };

  GLuint VAO, VBO;
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);

  glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), &cubeVertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
  glEnableVertexAttribArray(0);

  //      +-------------------------+
  //      |     UNIFORM BUFFER      |
  //      +-------------------------+
  GLuint uniform_index_red_sh = glGetUniformBlockIndex(shader_red.program, "Matrices");
  GLuint uniform_index_green_sh = glGetUniformBlockIndex(shader_green.program, "Matrices");
  GLuint uniform_index_blue_sh = glGetUniformBlockIndex(shader_blue.program, "Matrices");
  GLuint uniform_index_yellow_sh = glGetUniformBlockIndex(shader_yellow.program, "Matrices");

  glUniformBlockBinding(shader_red.program, uniform_index_red_sh, 0);
  glUniformBlockBinding(shader_green.program, uniform_index_green_sh, 0);
  glUniformBlockBinding(shader_blue.program, uniform_index_blue_sh, 0);
  glUniformBlockBinding(shader_yellow.program, uniform_index_yellow_sh, 0);

  GLuint ubo_matrices;
  glGenBuffers(1, &ubo_matrices);
  glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
  glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL, GL_STATIC_DRAW);
  // define the range of the buffer that links to a uniform binding point
  glBindBufferRange(GL_UNIFORM_BUFFER, 0, ubo_matrices, 0, 2 * sizeof(glm::mat4));

  // store the projection matrix (we only do this once now) (note: we're not using zoom anymore by changing the FoV)
  glm::mat4 projection = glm::perspective(45.0f, (float)width / (float)height, 0.1f, 100.0f);
  glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(projection));
  glBindBuffer(GL_UNIFORM_BUFFER, 0);

  //  camera.position = glm::vec3(3.9f, 2.12f, 7.94f);
  //  camera.yaw = -88;
  //  camera.pitch = -13.25;
  //  camera.check_mouse_movement(0, 0);

  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);

  //      +-------------------------+
  //      |         Game loop       |
  //      +-------------------------+
  while (!glfwWindowShouldClose(window))
  {
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
    glfwPollEvents();
    Do_Movement();

    // Clear the color buffer
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set the view and projection matrix in the uniform block - we only have to do this once per loop iteration.
    glm::mat4 view = camera.get_look_at_matrix();
    glBindBuffer(GL_UNIFORM_BUFFER, ubo_matrices);
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    // draw 4 cubes
    // RED
    glBindVertexArray(VAO);
    shader_red.use();
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(-0.75f, 0.75f, 0.0f));  // move top-left
    shader_red.set_mat4("model", model);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    // GREEN
    shader_green.use();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.75f, 0.75f, 0.0f));  // move top-right
    shader_green.set_mat4("model", model);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    // YELLOW
    shader_yellow.use();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(-0.75f, -0.75f, 0.0f));  // move bottom-left
    shader_yellow.set_mat4("model", model);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    // BLUE
    shader_blue.use();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.75f, -0.75f, 0.0f));  // move bottom-right
    shader_blue.set_mat4("model", model);
    glDrawArrays(GL_TRIANGLES, 0, 36);

    // Swap the screen buffers
    glfwSwapBuffers(window);

    //    +-------------------------+
    //    |   LOG                   |
    //    +-------------------------+
    //    camera.print_position();
  }

  // Terminate GLFW, clearing any resources allocated by GLFW.
  glfwTerminate();
  return 0;
}

GLuint make_texture(const char* path)
{
  GLuint texture;
  glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load!!!!!!" << std::endl;
    stbi_image_free(data);
  }

  return texture;
}

// Moves/alters the camera positions based on user input
void Do_Movement()
{
  // Camera controls
  if (keys[GLFW_KEY_W])
  {
    camera.check_keyboard(camera_forward, deltaTime);
  }
  if (keys[GLFW_KEY_S])
  {
    camera.check_keyboard(camera_backward, deltaTime);
  }
  if (keys[GLFW_KEY_A])
  {
    camera.check_keyboard(camera_left, deltaTime);
  }
  if (keys[GLFW_KEY_D])
  {
    camera.check_keyboard(camera_right, deltaTime);
  }

  if (keys[GLFW_KEY_UP])
  {
    if (blend_texture < 1.0f)
      blend_texture += 0.01f;
    else
      blend_texture = 1.0f;
  }
  if (keys[GLFW_KEY_DOWN])
  {
    if (blend_texture > 0.0f)
      blend_texture -= 0.01f;
    else
      blend_texture = 0.0f;
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  if (key >= 0 && key < 1024)
  {
    if (action == GLFW_PRESS)
      keys[key] = true;
    else if (action == GLFW_RELEASE)
      keys[key] = false;
  }
}

void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{
  camera.check_mouse_scroll(yoffset);
}

void mouse_callback(GLFWwindow* /*window*/, double xpos, double ypos)
{
  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

  lastX = xpos;
  lastY = ypos;

  camera.check_mouse_movement(xoffset, yoffset);
}

GLuint loadCubemap(std::vector<std::string> faces)
{
  unsigned int textureID;
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

  int width, height, nrChannels;
  for (unsigned int i = 0; i < faces.size(); i++)
  {
    unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
    if (data)
    {
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
      stbi_image_free(data);
    }
    else
    {
      std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
      stbi_image_free(data);
    }
  }
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  return textureID;
}
