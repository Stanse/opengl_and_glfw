#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;


in VS_OUT {
    vec2 texCoords;
    vec3 g_normal;
} gs_in[];

out vec3 object_normal;
out vec2 texture_coordinates;

uniform float time;

vec4 explode(vec4 position, vec3 normal)
{
    float magnitude = 2.0;
    vec3 direction = normal * ((sin(time) + 1.0) / 2.0) * magnitude;
    return position + vec4(direction, 0.0);
}

vec3 GetNormal()
{
    vec3 a = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
    vec3 b = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
    return normalize(cross(a, b));
}

void main() {
    vec3 normal = GetNormal();

    gl_Position = explode(gl_in[0].gl_Position, normal);
    texture_coordinates = gs_in[0].texCoords;
    object_normal = gs_in[0].g_normal;
    EmitVertex();
    gl_Position = explode(gl_in[1].gl_Position, normal);
    texture_coordinates = gs_in[1].texCoords;
    object_normal = gs_in[1].g_normal;
    EmitVertex();
    gl_Position = explode(gl_in[2].gl_Position, normal);
    texture_coordinates = gs_in[2].texCoords;
    object_normal = gs_in[2].g_normal;
    EmitVertex();
    EndPrimitive();
}

