#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include <GLFW/glfw3.h>
#include <map>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();
GLuint loadCubemap(std::vector<std::string> faces);
// GLuint make_texture(const char* path);

GLboolean debug_mode = false;

// Window dimensions
const GLuint width = 1400, height = 800;
GLfloat blend_texture = 0.1f;

// Camera
Camera camera(glm::vec3(10.0f, 0.0f, 10.0f));
bool keys[1024];
GLdouble lastX = 400, lastY = 300;
bool firstMouse = false;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

glm::vec3 light_color(1.0f, 1.0f, 1.0f);
glm::vec3 global_light_pos(4.0f, 2.0f, 4.0f);

// The MAIN function, from here we start the application and run the game loop
int main()
{
  // Init GLFW
  glfwInit();
  // Set all the required options for GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  glfwWindowHint(GLFW_SAMPLES, 16);  // MSAA Anti aliasing

  // Create a GLFWwindow object that we can use for GLFW's functions
  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || 5.1.0 Blinn-Phong model", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  // Options
  //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
  glewExperimental = GL_TRUE;
  // Initialize GLEW to setup the OpenGL Function pointers
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, width, height);

  // Build and compile our shader program
  //  Shader shader("Shaders/phong.vs", "Shaders/phong.fs");
  Shader shader("Shaders/blinn-phong.vs", "Shaders/blinn-phong.fs");

  Shader shader_debug("Shaders/shader_debug.vs", "Shaders/shader_debug.fs", "Shaders/shader_debug.gs");
  Shader shader_single_color("Shaders/single_color.vs", "Shaders/single_color.fs");
  //      +-------------------------+
  //      |         Load models     |
  //      +-------------------------+

  Model cube("../models/cube/cube.obj");
  Model floor("../models/floor/floor.obj");
  Model sphere("../models/planet/planet.obj");

  camera.position = glm::vec3(6.1f, 5.3f, 17.94f);
  camera.yaw = -103;
  camera.pitch = -21.25;
  camera.check_mouse_movement(0, 0);

  //      +-------------------------+
  //      |  Global OpenGL settings |
  //      +-------------------------+
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_MULTISAMPLE);  // set MSAA

  //      +-------------------------+
  //      |         Game loop       |
  //      +-------------------------+
  while (!glfwWindowShouldClose(window))
  {
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
    glfwPollEvents();
    Do_Movement();

    // Clear the color buffer
    if (debug_mode)
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    }
    else
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //    +-------------------------+
    //    |   LIGHT SETTINGS        |
    //    +-------------------------+
    shader.use();
    shader.set_float("material.shininess", 64.0f);

    // directional light
    shader.set_vec3("direct_light.direction", -0.2f, -1.0f, -1.3f);

    shader.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("direct_light.diffuse", 0.4, 0.4, 0.4);  // darken the light a bit to fit the scene
    shader.set_vec3("direct_light.specular", 0.5f, 0.5f, 0.5f);

    // Point light
    shader.set_vec3("point_lights[0].position", global_light_pos.x, global_light_pos.y, global_light_pos.z);
    shader.set_vec3("point_lights[0].ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("point_lights[0].diffuse", 0.8f, 0.8f, 0.8f);
    shader.set_vec3("point_lights[0].specular", 1.0f, 1.0f, 1.0f);
    shader.set_float("point_lights[0].constant", 1.0f);
    shader.set_float("point_lights[0].linear", 0.09);
    shader.set_float("point_lights[0].quadratic", 0.032);

    //    +-------------------------+
    //    |   VIEW                  |
    //    +-------------------------+
    glm::mat4 view;
    view = camera.get_look_at_matrix();

    shader.use();
    shader.set_mat4("view", view);
    shader.set_vec3("view_pos", camera.position.x, camera.position.y, camera.position.z);

    //    +-------------------------+
    //    |   PROJECTION            |
    //    +-------------------------+
    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, (float)width / (float)height, 0.1f, 1000.0f);

    shader.use();
    shader.set_mat4("projection", projection);

    //    +-------------------------+
    //    |   MODELS                |
    //    +-------------------------+
    shader.use();

    GLfloat time = glfwGetTime();

    glm::mat4 floor_model(1.0f);
    floor_model = glm::translate(floor_model, glm::vec3(0.0f, 0.0f, 0.0f));
    floor_model = glm::scale(floor_model, glm::vec3(2.0f));
    shader.set_mat4("model", floor_model);
    floor.draw(shader);

    // Light
    glm::mat4 light_model_pos(1.0f);
    light_model_pos = glm::translate(light_model_pos, global_light_pos);
    light_model_pos = glm::scale(light_model_pos, glm::vec3(0.3f));

    shader_single_color.use();
    shader_single_color.set_mat4("projection", projection);
    shader_single_color.set_mat4("view", view);
    shader_single_color.set_mat4("model", light_model_pos);
    sphere.draw(shader_single_color);

    global_light_pos.x = 5 * sin(time);
    global_light_pos.z = 5 * cos(time);

    // Swap the screen buffers
    glfwSwapBuffers(window);

    //    +-------------------------+
    //    |   LOG                   |
    //    +-------------------------+
    if (debug_mode)
      camera.print_position();
  }

  // Terminate GLFW, clearing any resources allocated by GLFW.
  glfwTerminate();
  return 0;
}

GLuint make_texture(const char* path)
{
  GLuint texture;
  glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load!!!!!!" << std::endl;
    stbi_image_free(data);
  }

  return texture;
}

// Moves/alters the camera positions based on user input
void Do_Movement()
{
  // Camera controls
  if (keys[GLFW_KEY_W])
  {
    camera.check_keyboard(camera_forward, deltaTime);
  }
  if (keys[GLFW_KEY_S])
  {
    camera.check_keyboard(camera_backward, deltaTime);
  }
  if (keys[GLFW_KEY_A])
  {
    camera.check_keyboard(camera_left, deltaTime);
  }
  if (keys[GLFW_KEY_D])
  {
    camera.check_keyboard(camera_right, deltaTime);
  }

  if (keys[GLFW_KEY_F1])
  {
    debug_mode = false;
  }
  if (keys[GLFW_KEY_F2])
  {
    debug_mode = true;
  }

  if (keys[GLFW_KEY_UP])
  {
    if (blend_texture < 1.0f)
      blend_texture += 0.01f;
    else
      blend_texture = 1.0f;
  }
  if (keys[GLFW_KEY_DOWN])
  {
    if (blend_texture > 0.0f)
      blend_texture -= 0.01f;
    else
      blend_texture = 0.0f;
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  if (key >= 0 && key < 1024)
  {
    if (action == GLFW_PRESS)
      keys[key] = true;
    else if (action == GLFW_RELEASE)
      keys[key] = false;
  }
}

void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{
  camera.check_mouse_scroll(yoffset);
}

void mouse_callback(GLFWwindow* /*window*/, double xpos, double ypos)
{
  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

  lastX = xpos;
  lastY = ypos;

  camera.check_mouse_movement(xoffset, yoffset);
}

GLuint loadCubemap(std::vector<std::string> faces)
{
  unsigned int textureID;
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

  int width, height, nrChannels;
  for (unsigned int i = 0; i < faces.size(); i++)
  {
    unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
    if (data)
    {
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
      stbi_image_free(data);
    }
    else
    {
      std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
      stbi_image_free(data);
    }
  }
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  return textureID;
}
