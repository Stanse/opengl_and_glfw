#version 330 core
layout (location = 0) in vec3 position;

out vec3 texture_coordinate;

uniform mat4 projection;
uniform mat4 view;

void main()
{
    texture_coordinate = position;
    vec4 pos = projection * view * vec4(position, 1.0f);
    gl_Position = vec4(pos.x, pos.y, pos.w, pos.w);
}
