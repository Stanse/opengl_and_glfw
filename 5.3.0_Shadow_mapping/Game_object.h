#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "Model.h"
#include "Shader.h"

class Game_object
{
public:
  Game_object(Model a_model) : model(a_model)
  {
    glm::mat4 model_position = glm::mat4(1.0f);
    model_position = glm::translate(model_position, glm::vec3(0.0));
  }

  void set_position(glm::vec3 position)
  {
    current_position = position;
  }

  void scale(glm::vec3 scale)
  {
    model_position = glm::scale(model_position, scale);
  }

  void draw(Shader& shader)
  {
    shader.use();
    model_position = glm::mat4(1.0);
    model_position = glm::translate(model_position, current_position);
    shader.set_mat4("model", model_position);
    model.draw(shader);
  }

  // private:
  Model model;
  glm::mat4 model_position;
  glm::vec3 current_position;
};

#endif  // GAME_OBJECT_H
