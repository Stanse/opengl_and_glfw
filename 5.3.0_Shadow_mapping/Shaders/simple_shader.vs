#version 330 core

layout (location = 0) in vec2 a_position;
layout (location = 1) in vec2 a_texture_coordinate;

out vec2 texture_coordinate;

void main()
{
    texture_coordinate = a_texture_coordinate;
    gl_Position = vec4(a_position, 0.0f, 1.0f);
}
