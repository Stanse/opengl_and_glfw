#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "Model.h"
#include "Shader.h"

class Game_object
{
public:
  Model model;
  glm::vec3 position;
  glm::vec3 scale;

  Game_object(Model a_model, glm::vec3 a_position = glm::vec3(0))
  {
    if (a_model.empty_model)
    {
      std::cout << "WARNING: model is empty!" << std::endl;
    }
    model = a_model;
    position = a_position;
    scale = glm::vec3(1.0, 1.0, 1.0);
  }

  void set_position(glm::vec3 a_position)
  {
    position = a_position;
  }

  void set_scale(GLfloat a_scale)
  {
    scale = glm::vec3(a_scale, a_scale, a_scale);
  }

  void render(Shader& shader)
  {
    glm::mat4 model_position(1);
    model_position = glm::translate(model_position, position);
    model_position = glm::scale(model_position, scale);
    shader.use();
    shader.set_mat4("model", model_position);
    model.draw(shader);
  }

  void render_primitive(Shader& shader)
  {
    shader.use();
    glm::mat4 model_position(1);
    model_position = glm::translate(model_position, position);
    model_position = glm::scale(model_position, scale);
    shader.set_mat4("model", model_position);

    //    shader.set_int("diffuseTexture", 0);
    //    shader.set_int("shadowMap", 1);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model.meshes[0].textures[0].id);
    //    glActiveTexture(GL_TEXTURE1);
    //    glBindTexture(GL_TEXTURE_2D, model.meshes[0].textures[1].id);

    glBindVertexArray(model.meshes[0].VAO);
    //  glDrawArrays(GL_TRIANGLES, 0, 24);
    glDrawElements(GL_TRIANGLES, model.meshes[0].indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
  }
};

#endif  // GAME_OBJECT_H
