#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Game_object.h"
#include <GLFW/glfw3.h>
#include <map>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
GLFWwindow* get_window();
void Do_Movement();
GLuint loadCubemap(std::vector<std::string> faces);
GLvoid directional_light_turn_on(Shader& shader, GLboolean state);
GLvoid point_light_turn_on(Shader& shader, GLboolean state);
GLuint make_depth_map(GLuint& Texture_depth_map);
GLvoid render_quad();
// GLuint make_texture(const char* path);

// Settings
const GLuint width = 1400, height = 800;
GLuint width_shadow = 1024, height_shadow = 1024;
GLfloat near_plane = -6.0f, far_plane = 17.5f;
unsigned int quadVAO = 0;
unsigned int quadVBO;
GLint debug_mode = -1;

// Camera
Camera camera(glm::vec3(10.0f, 0.0f, 10.0f));
bool keys[1024];
GLdouble lastX = 400, lastY = 300;
bool firstMouse = false;

// timing
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

// Lighting
glm::vec3 light_color(1.0f, 1.0f, 1.0f);
// glm::vec3 global_light_pos(4.0f, 2.0f, 4.0f);

glm::vec3 global_light_pos(3.0f, 6.0f, -4.0f);
GLfloat shininess = 64.0f;

// The MAIN function, from here we start the application and run the game loop
int main()
{
  GLFWwindow* window = get_window();

  //  | Build and compile our shader program
  //  --------------------------------------
  //  Shader shader("Shaders/blinn-phong.vs", "Shaders/blinn-phong.fs");
  Shader shader("Shaders/shadow_mapping.vs", "Shaders/shadow_mapping.fs");
  Shader shader_single_color("Shaders/single_color.vs", "Shaders/single_color.fs");
  Shader shader_depth_map("Shaders/depth_map.vs", "Shaders/depth_map.fs");
  Shader shader_debug_depth_map("Shaders/debug_depth.vs", "Shaders/debug_depth.fs");

  //  | Load models
  //  -------------------------
  Model model_cube("../models/cube/cube.obj");
  Model model_floor("../models/floor/floor.obj");
  Model model_sphere("../models/planet/planet.obj");

  Game_object cube(model_cube);
  Game_object floor(model_floor);
  Game_object sphere(model_sphere);

  //  | configure depth map FBO
  //  -------------------------
  GLuint depth_map;
  GLuint depth_map_fbo = make_depth_map(depth_map);

  camera.position = glm::vec3(4.25f, 6.2f, 16.3f);
  camera.yaw = -103;
  camera.pitch = -22;
  camera.check_mouse_movement(0, 0);

  //  |  Global OpenGL settings
  //  --------------------------
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_MULTISAMPLE);  // set MSAA
  glEnable(GL_CULL_FACE);

  //      +-------------------------+
  //      |         Game loop       |
  //      +-------------------------+
  while (!glfwWindowShouldClose(window))
  {
    GLfloat currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    GLfloat time = glfwGetTime();

    // Check if any events have been activated (key pressed, mouse moved etc.)
    glfwPollEvents();
    Do_Movement();

    // Clear the color buffer
    if (debug_mode == 1)
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glClearColor(.75f, .75f, .75f, 1.0f);
    }
    else if (debug_mode == -1)
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //    +-------------------------+
    //    |   LIGHT SETTINGS        |
    //    +-------------------------+
    // Point light
    global_light_pos.x = 5 * sin(time);
    global_light_pos.z = 5 * cos(time);

    //    directional_light_turn_on(shader, true);
    //    point_light_turn_on(shader, false);

    // shader configuration
    // --------------------
    shader_debug_depth_map.use();
    shader_debug_depth_map.set_int("depth_map", 0);
    shader.use();
    shader.set_int("diffuseTexture", 0);
    shader.set_int("shadowMap", 5);

    //    +-------------------------+
    //    |  PROJECTION AND VIEW    |
    //    +-------------------------+
    glm::mat4 lightProjection = glm::ortho(-20.0f, 20.0f, -20.0f, 20.0f, near_plane, far_plane);
    glm::mat4 lightView = glm::lookAt(global_light_pos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
    glm::mat4 light_space_matrix = lightProjection * lightView;

    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, (float)width / (float)height, 0.1f, 1000.0f);

    glm::mat4 view;
    view = camera.get_look_at_matrix();

    // 1. render depth of scene to texture (from light's perspective)
    // --------------------------------------------------------------

    shader_depth_map.use();
    shader_depth_map.set_mat4("light_space_matrix", light_space_matrix);

    glViewport(0, 0, width_shadow, height_shadow);
    glBindFramebuffer(GL_FRAMEBUFFER, depth_map_fbo);
    glClear(GL_DEPTH_BUFFER_BIT);

    // floor
    floor.set_position(glm::vec3(0));
    floor.set_scale(2.0);
    floor.render(shader_depth_map);

    //    glCullFace(GL_FRONT);

    cube.set_position(glm::vec3(1.0f, 1.25f, 2.0));
    cube.render(shader_depth_map);

    cube.set_position(glm::vec3(4.0f, 3.35f, -3.5));
    cube.render(shader_depth_map);

    // Model Spot Light
    sphere.set_position(global_light_pos);
    sphere.set_scale(0.3);
    sphere.render(shader_depth_map);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // reset viewport
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // 2. render scene as normal using the generated depth/shadow map
    // --------------------------------------------------------------
    glCullFace(GL_BACK);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    shader.use();
    shader.set_mat4("projection", projection);
    shader.set_mat4("view", view);
    // set light uniforms
    shader.set_vec3("viewPos", camera.position);
    shader.set_vec3("lightPos", global_light_pos);
    shader.set_mat4("lightSpaceMatrix", light_space_matrix);
    //    glActiveTexture(GL_TEXTUREs0);
    //    glBindTexture(GL_TEXTURE_2D, model_floor.meshes[0].textures[0].id);
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, depth_map);
    floor.render(shader);

    //    glActiveTexture(GL_TEXTURE0);
    //    glBindTexture(GL_TEXTURE_2D, model_cube.meshes[0].textures[0].id);
    cube.set_position(glm::vec3(1.0f, 1.25f, 2.0));
    cube.render(shader);

    cube.set_position(glm::vec3(4.0f, 3.35f, -3.5));
    cube.render(shader);

    sphere.set_position(global_light_pos);
    sphere.set_scale(0.3);
    sphere.render(shader);

    // render Depth map to quad for visual debugging
    // ---------------------------------------------
    //    shader_debug_depth_map.use();
    //    shader_debug_depth_map.set_float("near_plane", near_plane);
    //    shader_debug_depth_map.set_float("far_plane", far_plane);
    //    glActiveTexture(GL_TEXTURE0);
    //    glBindTexture(GL_TEXTURE_2D, depth_map);
    //    render_quad();

    // Swap the screen buffers
    glEnable(GL_FRAMEBUFFER_SRGB);  // Gamma correction
    glfwSwapBuffers(window);
    glDisable(GL_FRAMEBUFFER_SRGB);

    //    |   LOG
    //    +-------------------------+
    if (debug_mode == 1)
      camera.print_position();
  }

  // Terminate GLFW, clearing any resources allocated by GLFW.
  glfwTerminate();
  return 0;
}

GLuint make_texture(const char* path)
{
  GLuint texture;
  glGenTextures(1, &texture);
  GLint t_width, t_height, nr_components;
  unsigned char* data = stbi_load(path, &t_width, &t_height, &nr_components, 0);
  if (data)
  {
    GLenum format = 0;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, format, t_width, t_height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);
  }
  else
  {
    std::cerr << "ERROR: Texture failed to load!!!!!!" << std::endl;
    stbi_image_free(data);
  }

  return texture;
}

GLuint make_depth_map(GLuint& Texture_depth_map)
{
  GLuint depth_map_fbo;
  glGenFramebuffers(1, &depth_map_fbo);
  glGenTextures(1, &Texture_depth_map);
  glBindTexture(GL_TEXTURE_2D, Texture_depth_map);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width_shadow, height_shadow, 0, GL_DEPTH_COMPONENT, GL_FLOAT,
               NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

  // attach depth texture as FBO's depth buffer
  glBindFramebuffer(GL_FRAMEBUFFER, depth_map_fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, Texture_depth_map, 0);
  glDrawBuffer(GL_NONE);
  glReadBuffer(GL_NONE);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  return depth_map_fbo;
}

// renderQuad() renders a 1x1 XY quad in NDC
// -----------------------------------------
GLvoid render_quad()
{
  if (quadVAO == 0)
  {
    float quadVertices[] = {
      // positions        // texture Coords
      -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
      1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 1.0f,  -1.0f, 0.0f, 1.0f, 0.0f,
    };
    // setup plane VAO
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
  }
  glBindVertexArray(quadVAO);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindVertexArray(0);
}

// Moves/alters the camera positions based on user input
void Do_Movement()
{
  // Camera controls
  if (keys[GLFW_KEY_W])
  {
    camera.check_keyboard(camera_forward, deltaTime);
  }
  if (keys[GLFW_KEY_S])
  {
    camera.check_keyboard(camera_backward, deltaTime);
  }
  if (keys[GLFW_KEY_A])
  {
    camera.check_keyboard(camera_left, deltaTime);
  }
  if (keys[GLFW_KEY_D])
  {
    camera.check_keyboard(camera_right, deltaTime);
  }
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  if (key == GLFW_KEY_F1 && action == GLFW_PRESS)
    debug_mode = -1 * debug_mode;

  if (key >= 0 && key < 1024)
  {
    if (action == GLFW_PRESS)
      keys[key] = true;
    else if (action == GLFW_RELEASE)
      keys[key] = false;
  }
}

void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{
  camera.check_mouse_scroll(yoffset);
}

void mouse_callback(GLFWwindow* /*window*/, double xpos, double ypos)
{
  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

  lastX = xpos;
  lastY = ypos;

  camera.check_mouse_movement(xoffset, yoffset);
}

GLuint loadCubemap(std::vector<std::string> faces)
{
  unsigned int textureID;
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

  int width, height, nrChannels;
  for (unsigned int i = 0; i < faces.size(); i++)
  {
    unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
    if (data)
    {
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
      stbi_image_free(data);
    }
    else
    {
      std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
      stbi_image_free(data);
    }
  }
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  return textureID;
}

GLvoid directional_light_turn_on(Shader& shader, GLboolean state)
{
  if (state)
  {
    shader.use();
    shader.set_float("material.shininess", shininess);

    shader.set_vec3("direct_light.direction", global_light_pos);

    shader.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("direct_light.diffuse", 0.4, 0.4, 0.4);  // darken the light a bit to fit the scene
    shader.set_vec3("direct_light.specular", 0.01f, 0.5f, 0.5f);
  }
  else
  {
    shader.set_vec3("direct_light.ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("direct_light.diffuse", 0.000, 0.004, 0.004);  // darken the light a bit to fit the scene
    shader.set_vec3("direct_light.specular", 0.001f, 0.005f, 0.005f);
  }
}

GLvoid point_light_turn_on(Shader& shader, GLboolean state)
{
  shader.use();

  shader.set_vec3("point_lights[0].position", global_light_pos);
  if (state)
  {
    shader.set_vec3("point_lights[0].ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("point_lights[0].diffuse", 0.8f, 0.8f, 0.8f);
    shader.set_vec3("point_lights[0].specular", 0.01f, .5f, .5f);
    shader.set_float("point_lights[0].constant", 1.0f);
    shader.set_float("point_lights[0].linear", 0.09);
    shader.set_float("point_lights[0].quadratic", 0.032);
  }
  else
  {
    shader.set_vec3("point_lights[0].ambient", 0.05f, 0.05f, 0.05f);
    shader.set_vec3("point_lights[0].diffuse", 0.000, 0.008f, 0.008f);
    shader.set_vec3("point_lights[0].specular", 0.001f, .005f, .005f);
    shader.set_float("point_lights[0].constant", 1.0f);
    shader.set_float("point_lights[0].linear", 0.09);
    shader.set_float("point_lights[0].quadratic", 0.032);
  }
}

GLFWwindow* get_window()
{
  // Init GLFW
  glfwInit();
  // Set all the required options for GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  glfwWindowHint(GLFW_SAMPLES, 16);  // MSAA Anti aliasing

  // Create a GLFWwindow object that we can use for GLFW's functions
  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || 5.3.1 Shadow mapping", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  // Options
  //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
  glewExperimental = GL_TRUE;
  // Initialize GLEW to setup the OpenGL Function pointers
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, width, height);

  return window;
}
