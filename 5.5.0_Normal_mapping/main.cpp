#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Game_object.h"
#include <GLFW/glfw3.h>
#include <map>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "../glm/glm.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_ptr.hpp"

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();
GLFWwindow* get_window();

unsigned int loadTexture(const char* path);
void renderScene(Shader& shader, Game_object cube, Game_object floor, Game_object wall);
void renderCube();
GLuint make_depth_map(GLuint& depth_cube_map);

// settings
const unsigned int width = 1400;
const unsigned int height = 900;
const unsigned int width_shadow = 1024, height_shadow = 1024;

static GLint debug_mode = -1;

// Camera
static Camera camera(glm::vec3(10.0f, 0.0f, 10.0f));
static bool keys[1024];
static GLdouble lastX = 400, lastY = 300;

// timing
static float deltaTime = 0.0f;
static float lastFrame = 0.0f;

// Lighting
static glm::vec3 light_color(1.0f, 1.0f, 1.0f);

static glm::vec3 point_light_pos(0.0f, 4.0f, 0.0f);

int main()
{
  // glfw: initialize and configure
  // ------------------------------
  GLFWwindow* window = get_window();

  camera.position = glm::vec3(1.22f, 3.4f, 1.0f);
  camera.yaw = -72;
  camera.pitch = -39;
  camera.check_mouse_movement(0, 0);

  // build and compile shaders
  // -------------------------
  Shader shader("Shaders/shadow_mapping1.vs", "Shaders/shadow_mapping1.fs");
  //  Shader shader("Shaders/shadow_mapping.vs", "Shaders/shadow_mapping.fs");
  Shader shader_depth_map("Shaders/depth_map.vs", "Shaders/depth_map.fs", "Shaders/depth_map.gs");
  Shader shader_single_color("Shaders/single_color.vs", "Shaders/single_color.fs");

  //  | Load models
  //  -------------------------
  Model model_cube("../models/cube/cube.obj");
  Model model_floor("../models/floor/floor.obj");
  Model model_wall("../models/wall/wall.obj");
  Model model_sphere("../models/planet/planet.obj");
  Model model_writer("../models/writer/writer.obj");

  Game_object cube(model_cube);
  Game_object floor(model_floor);
  Game_object wall(model_wall);
  Game_object sphere(model_sphere);
  Game_object writer(model_writer);

  // configure depth map FBO
  // -----------------------
  unsigned int depth_map_fbo;
  // create depth cubemap texture
  unsigned int depth_cube_map;

  depth_map_fbo = make_depth_map(depth_cube_map);

  // shader configuration
  // --------------------
  shader.use();
  //  shader.set_int("diffuseTexture", 0);
  shader.set_int("depthMap", 5);

  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  // render loop
  // -----------
  while (!glfwWindowShouldClose(window))
  {
    // per-frame time logic
    // --------------------
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // input
    // -----
    glfwPollEvents();
    Do_Movement();

    // move light position over time
    //    point_light_pos.x = sin(glfwGetTime() * 0.8) * 2.0;
    //    point_light_pos.z = cos(glfwGetTime() * 0.8) * 2.0;

    // Clear the color buffer
    if (debug_mode == 1)
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glClearColor(.75f, .75f, .75f, 1.0f);
    }
    else if (debug_mode == -1)
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    }

    //    +-------------------------+
    //    |  PROJECTION AND VIEW    |
    //    +-------------------------+
    glm::mat4 projection;
    projection = glm::perspective(camera.zoom, (float)width / (float)height, 0.1f, 100.0f);
    glm::mat4 view;
    view = camera.get_look_at_matrix();

    // 0. create depth cubemap transformation matrices
    // -----------------------------------------------
    float near_plane = 1.0f;
    float far_plane = 25.0f;
    glm::mat4 shadowProj = glm::perspective(90.0f, (float)width_shadow / (float)height_shadow, near_plane, far_plane);
    std::vector<glm::mat4> shadowTransforms;
    shadowTransforms.push_back(shadowProj * glm::lookAt(point_light_pos, point_light_pos + glm::vec3(1.0f, 0.0f, 0.0f),
                                                        glm::vec3(0.0f, -1.0f, 0.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(point_light_pos, point_light_pos + glm::vec3(-1.0f, 0.0f, 0.0f),
                                                        glm::vec3(0.0f, -1.0f, 0.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(point_light_pos, point_light_pos + glm::vec3(0.0f, 1.0f, 0.0f),
                                                        glm::vec3(0.0f, 0.0f, 1.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(point_light_pos, point_light_pos + glm::vec3(0.0f, -1.0f, 0.0f),
                                                        glm::vec3(0.0f, 0.0f, -1.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(point_light_pos, point_light_pos + glm::vec3(0.0f, 0.0f, 1.0f),
                                                        glm::vec3(0.0f, -1.0f, 0.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(point_light_pos, point_light_pos + glm::vec3(0.0f, 0.0f, -1.0f),
                                                        glm::vec3(0.0f, -1.0f, 0.0f)));

    // 1. render scene to depth cubemap
    // --------------------------------
    glViewport(0, 0, width_shadow, height_shadow);
    glBindFramebuffer(GL_FRAMEBUFFER, depth_map_fbo);
    glClear(GL_DEPTH_BUFFER_BIT);
    shader_depth_map.use();
    for (unsigned int i = 0; i < 6; ++i)
    {
      shader_depth_map.set_mat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
    }
    shader_depth_map.set_float("far_plane", far_plane);
    shader_depth_map.set_vec3("lightPos", point_light_pos);

    // floor
    floor.set_position(glm::vec3(0.0, 0.0, 0.0));
    floor.set_scale(0.8);
    floor.render(shader_depth_map);

    wall.set_position(glm::vec3(5.0, 2.0, 0.0));
    wall.set_scale(0.5);
    //    wall.set_rotation(90, glm::vec3(0.0, 0.0, 1.0));
    wall.render(shader_depth_map);

    cube.set_position(glm::vec3(2.0f, 1.3f, -2.0));
    cube.set_scale(2.5);
    cube.render(shader_depth_map);

    writer.set_position(glm::vec3(2.2, 2.55, -2.2));
    writer.set_scale(0.18f);
    writer.render(shader_depth_map);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // 2. render scene as normal
    // -------------------------
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    shader.use();
    shader.set_mat4("projection", projection);
    shader.set_mat4("view", view);
    // set lighting uniforms
    shader.set_vec3("lightPos", point_light_pos);
    shader.set_vec3("viewPos", camera.position);
    shader.set_float("far_plane", far_plane);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_CUBE_MAP, depth_cube_map);

    // models
    floor.render(shader);
    wall.render(shader);
    cube.render(shader);
    writer.render(shader);

    // light
    shader_single_color.use();
    shader_single_color.set_mat4("projection", projection);
    shader_single_color.set_mat4("view", view);
    sphere.set_position(point_light_pos);
    sphere.set_scale(0.2f);
    sphere.render(shader_single_color);

    // glfw: swap buffers a
    // --------------------
    glfwSwapBuffers(window);

    //    |   LOG
    //    +-------------------------+
    if (debug_mode == 1)
      camera.print_position();
  }

  glfwTerminate();
  return 0;
}

// Moves/alters the camera positions based on user input
void Do_Movement()
{
  // Camera controls
  if (keys[GLFW_KEY_W])
  {
    camera.check_keyboard(camera_forward, deltaTime);
  }
  if (keys[GLFW_KEY_S])
  {
    camera.check_keyboard(camera_backward, deltaTime);
  }
  if (keys[GLFW_KEY_A])
  {
    camera.check_keyboard(camera_left, deltaTime);
  }
  if (keys[GLFW_KEY_D])
  {
    camera.check_keyboard(camera_right, deltaTime);
  }
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
  if (key == GLFW_KEY_F1 && action == GLFW_PRESS)
    debug_mode = -1 * debug_mode;

  if (key >= 0 && key < 1024)
  {
    if (action == GLFW_PRESS)
      keys[key] = true;
    else if (action == GLFW_RELEASE)
      keys[key] = false;
  }
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* /*window*/, double xpos, double ypos)
{
  GLfloat xoffset = xpos - lastX;
  GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

  lastX = xpos;
  lastY = ypos;

  camera.check_mouse_movement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{
  camera.check_mouse_scroll(yoffset);
}

GLuint make_depth_map(GLuint& depth_cube_map)
{
  GLuint depth_map_fbo;
  glGenFramebuffers(1, &depth_map_fbo);
  glGenTextures(1, &depth_cube_map);

  glBindTexture(GL_TEXTURE_CUBE_MAP, depth_cube_map);
  for (GLuint i = 0; i < 6; ++i)
  {
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, width_shadow, height_shadow, 0,
                 GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  }
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  // attach depth texture as FBO's depth buffer
  glBindFramebuffer(GL_FRAMEBUFFER, depth_map_fbo);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_cube_map, 0);
  glDrawBuffer(GL_NONE);
  glReadBuffer(GL_NONE);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;

  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  return depth_map_fbo;
}

GLFWwindow* get_window()
{
  // Init GLFW
  glfwInit();
  // Set all the required options for GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  glfwWindowHint(GLFW_SAMPLES, 16);  // MSAA Anti aliasing

  // Create a GLFWwindow object that we can use for GLFW's functions
  GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL core 3.3 || Test normal mapping", nullptr, nullptr);
  glfwMakeContextCurrent(window);

  // Options
  //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Set the required callback functions
  glfwSetKeyCallback(window, key_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);

  // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
  glewExperimental = GL_TRUE;
  // Initialize GLEW to setup the OpenGL Function pointers
  glewInit();

  // Define the viewport dimensions
  glViewport(0, 0, width, height);

  return window;
}
